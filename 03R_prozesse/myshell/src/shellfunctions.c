#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "plist.h"
#include "shellfunctions.h"
#include "shellutils.h"

// write your local helper functions here

//3.1
void read_input(char* const command_line, int size, FILE* stream)
{
    //durch das wait in der print Anweisungen wartet er jetzt wieder auf die Terminierung der Hintergrundprozesse (falsch)
    //trotzdem verschwindet der Zombie nicht aus der Liste
    //int status;
    //printf("%d\n", wait(&status));
    //system("ps u"); //Anzeige der laufenden Prozesse (Z steht für Zombie)


    printf("Command:");
    if( fgets(command_line, size, stream) != NULL ) {
        fputs(command_line, stream);
    } else {
        if(feof(stdin)) {
            printf("\nGoodbye!\n");
            exit(1);
        }
    }

}

//3.2
//TODO: Error bei falschem programm oder falschem argument. Über argv?
void execute_command(char* command_line)
{
    command_t *command = parse_command_line(command_line);
    //Check zombies

    prompt(command_line);

    if (command == NULL) {
        perror("NULL command");
        return;
    }

    if(strcmp(command_line, " ") == 0) {
        perror("Empty command error");
        return;
    }
    if (command->parse_error != NULL) {
        perror(command->parse_error);
        return;
    }

    //3.5 - Always exit?
    if(strcmp(command_line, "cd") == 0) {
        //Error handling
        if (command->argv[1] == NULL) {
            perror("No direcory selected!");
            exit(1);
        }
        if (command->argv[2] != NULL) {
            perror("To much arguments!");
            exit(1);
        }

        char* directory = command->argv[1]; //Only Linux NOT Windows(\)!

        if (chdir(directory) == -1) {
            perror("Failture while changing directory!");
            exit(1);
        }
    }
    if(strcmp(command_line, "jobs") == 0) {
        //läuft durch die verkette Liste durch und gibt die Hintergrundprozesse an die gestartet wurden
        walk_list(&callback);
    }

    pid_t pid;
    int status;
    //Fallunterscheidung
    switch (pid = fork()) {
        case -1:
            perror("Failture in fork");
            exit(1);
        case 0:
            // im Kindprozess

            printf("%s\n", command->command_line);

            if (execvp(command->argv[0], command->argv) < -1) {
                perror("Failture in execve");
            }

            //versucht terminierten Kindprozess aus der Liste zu entfernen
            //funktioniert noch nicht ganz (hab das mit den Buffern noch nicht ganz verstanden)
            remove_element(getpid(), command->command_line, sizeof(command->command_line));
            exit(1);
        default:
            // im Elternprozess
            if (command->background == 1) {
                //fügt laufenden Hintergrundprozess in die verkette Liste ein
                insert_element(pid, command_line);

                //No waiting needed
                return;
                //break;
            }
            if (waitpid(pid, &status, 0) != pid) {
                perror("Failture in waitpid");
                exit(1);
            }
            print_status(command_line, status);


            break;
    }

    free(command);
}

void collect_defunct_process()
{
    // Write your code here
}

//callback Funktion, wird der walk_list in plist.c übergeben
//muss 0 returnen, damit die Liste weiterdurchgelaufen wird
int callback(pid_t pid, const char* charsen) {
    printf("PID: %d - %s\n", pid, charsen);
    return 0;
}
