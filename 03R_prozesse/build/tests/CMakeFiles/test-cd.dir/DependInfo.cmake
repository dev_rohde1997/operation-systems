# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/carl/Documents/Betriebssysteme/team_084/03R_prozesse/tests/src/test-cd.c" "/Users/carl/Documents/Betriebssysteme/team_084/03R_prozesse/build/tests/CMakeFiles/test-cd.dir/src/test-cd.c.o"
  )
set(CMAKE_C_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "_XOPEN_SOURCE"
  "_XOPEN_SOURCE_EXTENDED"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../myshell/include"
  "../tests/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
