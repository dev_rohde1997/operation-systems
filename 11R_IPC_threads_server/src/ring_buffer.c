#include "../include/ring_buffer.h"
#include "../include/reads_list.h"
#include "../include/server.h"

#include "pthread.h"
#include "semaphore.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "time.h"

typedef struct ring_buffer_element
{
    //! HINT: something is missing here
    char  text[MAX_MESSAGE_LENGTH];
    int   reader_count; //writes

} ring_buffer_element_t;

static ring_buffer_element_t ring_buffer[RINGBUFFER_SIZE];

unsigned int current_writer = 0;
unsigned int number_of_readers = 0;

int init = 1;
//synchronization variables

pthread_rwlock_t  read_lock = PTHREAD_RWLOCK_INITIALIZER;

pthread_mutex_t num_of_reader_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t current_writer_mutex = PTHREAD_MUTEX_INITIALIZER;

pthread_rwlock_t rwlock_temp = PTHREAD_RWLOCK_INITIALIZER;

sem_t writes;


//-----------------------------------------------------------------------------

int ringbuffer_write_element(char* text)
{
    //! HINTS: Check if thread can read a new element, and synchronization will be needed

    if (init) {
        sem_init(&writes, 0, RINGBUFFER_SIZE);
        init = 0;
    }

    pthread_mutex_lock(&current_writer_mutex);



    pthread_rwlock_wrlock(&rwlock_temp);

    if (ring_buffer[current_writer % RINGBUFFER_SIZE].reader_count == 0)
    {
        struct timespec ts;

        if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
            perror("clock_gettime");
        }
        ts.tv_sec += 1;
        printf("ringbuffer_write_element : sem_ring_buffer timedwait\n");
        //if(sem_timedwait(&ring_buffer[current_writer % RINGBUFFER_SIZE].sem_ring_buffer, &ts) < 0) {
        if(sem_timedwait(&writes, &ts) < 0) {
            return -1;
        }
        //! Write element
        strcpy(ring_buffer[current_writer % RINGBUFFER_SIZE].text, text);

    }

    pthread_rwlock_unlock(&rwlock_temp);

    pthread_mutex_lock(&num_of_reader_mutex);
    ring_buffer[current_writer % RINGBUFFER_SIZE].reader_count = number_of_readers;
    pthread_mutex_unlock(&num_of_reader_mutex);

    //sem_wait(&writes);

    reads_list_increment_all();

    current_writer++;

    pthread_mutex_unlock(&current_writer_mutex);

    return 0;
}

//-----------------------------------------------------------------------------

void ringbuffer_read_element(int* current_reader, char* buffer, unsigned int client_number)
{
    //! HINT: Check if thread can read a new element & synchronization will be needed

    int reader = *current_reader;
    //! HINT: Check if thread can read a new element & synchronization will be needed

    //ring_buffer_element_t *read_element = &ring_buffer[reader % RINGBUFFER_SIZE];

    printf("            aktuelle reads : %d\n", reads_list_get_reads(client_number));


    if((unsigned int)*current_reader >= current_writer
       && reads_list_get_reads(client_number) == 0)
    {
        strcpy(buffer, "nack"); //copy "nack" message to

        return;
    }
    //printf("            aktuelle reads : %d\n", reads_list_get_reads(client_number));

    pthread_rwlock_wrlock(&rwlock_temp); //write lock so reader count can decrement
    ring_buffer[reader % RINGBUFFER_SIZE].reader_count--;
    pthread_rwlock_unlock(&rwlock_temp);

    reads_list_decrement(client_number);

    pthread_rwlock_rdlock(&read_lock);
    //! Read Element
    strcpy(buffer, (const char*)ring_buffer[reader % RINGBUFFER_SIZE].text);
    if(ring_buffer[reader % RINGBUFFER_SIZE].reader_count == 0) {
        sem_post(&writes);
    }
    //! HINT: notify the writer
    pthread_rwlock_unlock(&read_lock); //unlock ->reading finished

    //! Update reader count
    (*current_reader)++;






    /*

    int reader = *current_reader;

    if((unsigned int)*current_reader >= current_writer)
    {
        strcpy(buffer, "nack"); //copy "nack" message to buffer
        return;
    }

    struct timespec ts;
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
        perror("clock_gettime");
    }
    ts.tv_sec += 1;
    if(sem_timedwait(&sem_has_written, &ts) < 0) {
        strcpy(buffer, (const char*)"nack");
        return;
    }


    if (ring_buffer[reader % RINGBUFFER_SIZE].reader_count == 0)
    {
        strcpy(buffer, (const char*)"nack");
        return;
    }


    //! Read Element
    strcpy(buffer, (const char*)ring_buffer[reader % RINGBUFFER_SIZE].text);

    ring_buffer[reader % RINGBUFFER_SIZE].reader_count--;
    reads_list_decrement(client_number);


    if (ring_buffer[reader % RINGBUFFER_SIZE].reader_count == 0) {
        printf("ringbuffer_read_element : sem_write post\n");
        sem_post(&writes);
        //Sem aus list
    }


    strcpy(buffer, (const char*)ring_buffer[reader % RINGBUFFER_SIZE].text);

    //! Update reader count
    (*current_reader)++;

    //notify the writer
    printf("ringbuffer_read_element : writes POST\n");
    //sem_post(&ring_buffer[current_writer % RINGBUFFER_SIZE].sem_ring_buffer);



    if (sem_post(&writes) < 0 )
        {
            printf("Error. Semaphore couldn't be incremented!\n");
            exit(1);
        }
    */
    return;
}
/*
void ringbuffer_read_element(int* current_reader, char* buffer, unsigned int client_number)
{
    int reader = *current_reader;
    //! HINT: Check if thread can read a new element & synchronization will be needed

    ring_buffer_element_t *read_element = &ring_buffer[reader % RINGBUFFER_SIZE];

    if((unsigned int)*current_reader >= current_writer)
    {
        strcpy(buffer, "nack"); //copy "nack" message to buffer
        return;
    }

    if (pthread_rwlock_wrlock(&read_element->lock_2) < 0)//write lock so reader count can decrement
    {
        printf("Error. Writer couldn't be locked!\n");
        exit(1);
    }
    ring_buffer[reader % RINGBUFFER_SIZE].reader_count--;
    if (pthread_rwlock_unlock(&read_element->lock_2) < 0) //unlock
    {
        printf("Error. Unlocking failed!\n");
        exit(1);
    }

    reads_list_decrement(client_number);

    if (pthread_rwlock_rdlock(&read_element->lock_2) < 0) //lock
    {
        printf("Error. Reader couldn't be locked!\n");
        exit(1);
    }
    //! Read Element
    strcpy(buffer, (const char*)ring_buffer[reader % RINGBUFFER_SIZE].text);

    //! HINT: notify the writer
    if (pthread_rwlock_unlock(&read_element->lock_2) < 0)//unlock ->reading finished
    {
        printf("Error. Unlocking failed!\n");
        exit(1);
    }
    //! Update reader count
    (*current_reader)++;

    return ;
}

*/
//-----------------------------------------------------------------------------

int ringbuffer_add_reader(unsigned int client_number)
{

    //! HINT: synchronization is needed in this function

    if(reads_list_insert_element(client_number) != 0)
    {
        exit(EXIT_FAILURE);
    }

    if (pthread_mutex_lock(&num_of_reader_mutex) != 0) //lock for save disconnecting of client
        {
            printf("Error. Reader couldn't be locked!\n");
            exit(1);
        }
    number_of_readers++;
    if (pthread_mutex_unlock(&num_of_reader_mutex) != 0) //unlock
    {
        printf("Error. Unlocking failed!\n");
        exit(1);
    }

    if (pthread_mutex_lock(&current_writer_mutex) != 0) //lock for save disconnecting of client
        {
            printf("Error. Reader couldn't be locked!\n");
            exit(1);
        }
    int new_reader = current_writer;
    if (pthread_mutex_unlock(&current_writer_mutex) != 0) //unlock
    {
        printf("Error. Unlocking failed!\n");
        exit(1);
    }
    return new_reader;
}

//-----------------------------------------------------------------------------

void ringbuffer_remove_reader(int* current_reader, unsigned int client_number)
{
    //! HINT: synchronization is needed in this function

    int reads = reads_list_get_reads(client_number);

    //! perform all unfinished reads for the disconnected client
    while(reads != 0)
    {
        char buffer[MAX_MESSAGE_LENGTH];
        ringbuffer_read_element(current_reader, buffer, client_number);
        reads = reads_list_get_reads(client_number);
    }

    reads_list_remove_reader(client_number);

    if (pthread_mutex_lock(&num_of_reader_mutex) != 0) //lock for save disconnecting of client
    {
        printf("Error. Reader couldn't be locked!\n");
        exit(1);
    }
    number_of_readers--;
    if (pthread_mutex_unlock(&num_of_reader_mutex) != 0) //unlock
    {
        printf("Error. Unlocking failed!\n");
        exit(1);
    }

    return;
}
