#include "reads_list.h"
#include "pthread.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct reads_list_element
{
    //! HINT: something is missing here
    unsigned int client_number;
    unsigned int reads;
    struct reads_list_element* next;
    struct reads_list_element* previous;
    sem_t reads_list_sem;

} reads_list_element_t;


reads_list_element_t* head = NULL;

//! HINT: maybe global synchronization variables are needed

pthread_rwlock_t  rwlock = PTHREAD_RWLOCK_INITIALIZER;

//-----------------------------------------------------------------------------

int reads_list_insert_element(unsigned int client_number)
{

    pthread_rwlock_wrlock(&rwlock);
    //! create new element
    reads_list_element_t* new_element = malloc(sizeof(reads_list_element_t));

    if(new_element == NULL)
    {
        perror("malloc");
        exit(EXIT_FAILURE);
    }

    new_element->client_number = client_number;
    new_element->reads = 0;

    //! insert element into list
    if(head == NULL)
    {
        new_element->next     = NULL;
        new_element->previous = NULL;
        head                  = new_element;
        pthread_rwlock_unlock(&rwlock);
        return 0;
    }

    reads_list_element_t* temporary = head;
    while(temporary->next != NULL)
    {
        temporary = temporary->next;
    }

    new_element->next     = NULL;
    new_element->previous = temporary;
    temporary->next       = new_element;

    pthread_rwlock_unlock(&rwlock);
    return 0;
}

//-----------------------------------------------------------------------------

sem_t* reads_list_get_reader_semaphore(unsigned int client_number)
{
    pthread_rwlock_rdlock(&rwlock);

    reads_list_element_t* temporary = head;
    while(temporary->client_number != client_number)
    {
        if (temporary->next == NULL)
        {
            perror("Client Number invalid");
            pthread_rwlock_unlock(&rwlock);
            exit(EXIT_FAILURE);
        }
        temporary = temporary->next;
    }

    pthread_rwlock_unlock(&rwlock);
    return &temporary->reads_list_sem;
}

//-----------------------------------------------------------------------------

void reads_list_increment_all()
{
    pthread_rwlock_wrlock(&rwlock);
    reads_list_element_t* temporary = head;

    while(temporary != NULL)
    {
        temporary->reads++;
        temporary = temporary->next;
    }
    pthread_rwlock_unlock(&rwlock);
}

//-----------------------------------------------------------------------------

void reads_list_decrement(unsigned int client_number)
{
    pthread_rwlock_wrlock(&rwlock);

    reads_list_element_t* temporary = head;

    while(temporary->client_number != client_number)
    {
        if (temporary->next == NULL)
        {
            perror("Client Number invalid");
            pthread_rwlock_unlock(&rwlock);
            exit(EXIT_FAILURE);
        }
        temporary = temporary->next;
    }
    temporary->reads--;
    pthread_rwlock_unlock(&rwlock);

}

//-----------------------------------------------------------------------------

int reads_list_remove_reader(unsigned int client_number)
{
    //! HINT: synchronization is needed in this function

    pthread_rwlock_wrlock(&rwlock);

    //! find element to remove
    reads_list_element_t* temporary = head;
    while(temporary != NULL && temporary->client_number != client_number)
    {
        temporary = temporary->next;
    }

    if(temporary == NULL)
    {
        pthread_rwlock_unlock(&rwlock);
        return -1;
    }

    //! bend pointers around element
    if(temporary->previous != NULL)
    {
        temporary->previous->next = temporary->next;
    }
    if(temporary->next != NULL)
    {
        temporary->next->previous = temporary->previous;
    }
    if(temporary == head)
    {
        head = temporary->next;
    }

    //! finally delete element
    free(temporary);

    pthread_rwlock_unlock(&rwlock);
    return 0;
}

//-----------------------------------------------------------------------------

int reads_list_get_reads(unsigned int client_number)
{
    pthread_rwlock_rdlock(&rwlock);
    reads_list_element_t* temporary = head;

    while(temporary->client_number != client_number)
        {
          if (temporary->next == NULL)
          {
              perror("Client Number invalid");
              pthread_rwlock_unlock(&rwlock);
              exit(EXIT_FAILURE);
              //Fehlerbehandlung beim Aufruf der Funktion beachten
          }
          temporary = temporary->next;
    }
    pthread_rwlock_unlock(&rwlock);
    return temporary->reads;
    
}
