#include "mymalloc.h"

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/types.h>
#include <unistd.h>

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "colorCodes.h"

chunk_t* root = NULL;
int fd;
memory_block_t* free_space_list_entry_point;
memory_block_t* memory_block;

//===================================================================================================================//

/**
 * @brief get_page_size returns page size in bytes
 * @return page size in byte
 */
size_t get_page_size(void)
{
    return sysconf(_SC_PAGESIZE);
}

//-------------------------------------------------------------------------------------------------------------------//

/**
 * @brief open_file: opens a file with the size of 1 page for that we want to manage the memory
 * @return file descriptor
 */
int open_file(void)
{
    fd = open(FILE_NAME, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    if (fd < 0)
    {
      perror("Failure while opening file");
      exit(1);
    }
    int err_code = posix_fallocate(fd, 0, get_page_size());
    if (err_code != 0)
    {
       perror("Failure while posix_fallocate");
       exit(1);
    }
    return fd;
}

//-------------------------------------------------------------------------------------------------------------------//

/**
 * @brief my_malloc allocates memory that is backed by a file
 * @param size: size of memory to allocate in bytes
 * @return pointer to allocated memory
 */
void* my_malloc(size_t size)
{
    //1. Check conditions
    //Define root

    if (root == NULL)
    {
        if((fd = open_file()) < 0)
        {
            perror("Failure while opening file");
            exit(1);
        }

        void* ret_mmap;

        if ((ret_mmap = mmap(START_ADDRESS, get_page_size(), PROT_READ | PROT_WRITE,
            MAP_FIXED|MAP_SHARED, fd, 0)) == MAP_FAILED)
        {
            perror("Failure while mmap");
            exit(1);
        }

        root = (chunk_t*) ret_mmap;
        root->file = fd;
        root->size = (get_page_size());

        memory_block = (memory_block_t*) (root + 1);
        memory_block->size = (size_t) (root->size - sizeof(memory_block_t) - sizeof(chunk_t));
        memory_block->next = NULL;
        memory_block->previous = NULL;

        root->free_space_list_entry_point = memory_block;
    }

    //Runden des zu reservierenden Speicherns
    --size;
    size /= sizeof(memory_block_t);
    ++size;
    size *= sizeof(memory_block_t);

    if (size == 0) {
      return NULL;
    }
    memory_block = root->free_space_list_entry_point;

    //2. Find free size
    while((int) (memory_block->size) < (int) size)
    {
        memory_block = memory_block->next;
        if (memory_block == NULL)
        {
            errno = ENOMEM;
            return NULL;
        }
    }

    //Pointer
    if (sizeof(memory_block_t) < (memory_block->size - size))
    {
        int puffer = size / sizeof(memory_block_t) + 1;
        memory_block_t* temp_block = memory_block + puffer;
        temp_block->previous = memory_block;
        temp_block->next = memory_block->next;

        if (temp_block->previous != NULL)
        {
            temp_block->previous->next = temp_block;
        }

        if (temp_block->next != NULL)
        {
            temp_block->next->previous = temp_block;
        }

        //Groesse aktualisieren
        temp_block->size = memory_block->size - size - sizeof(memory_block_t);
        memory_block->size = size;
    }

    if (memory_block->previous != NULL)
    {
        memory_block->previous->next = memory_block->next;
    }
    else
    {
        root->free_space_list_entry_point = memory_block->next;
    }

    if (memory_block->next != NULL)
    {
        memory_block->next->previous = memory_block->previous;
    }

    memory_block->previous = MAGIC_NUMBER;
    memory_block->next = MAGIC_NUMBER;
    return (void*) (memory_block + 1);
}

//-------------------------------------------------------------------------------------------------------------------//

/**
 * @brief my_free: releases memory that was allocated by my_free
 * @param memory_location: pointer to memory that should be freed
 */
void my_free(void* memory_location)
{

    //Überprüfe ob memory_location gültig ist
    if (memory_location == NULL ||  root == NULL)
    {
        return;
    }

    //Überprüfen ob der übergeben Pointer im richtigen Bereich liegt
    if (START_ADDRESS > memory_location ||
        memory_location > (void*)(get_page_size() + (char*) START_ADDRESS))
    {
        return;
    }

    //Berechnung des Pointers auf den dazugehörigen Memory_Block
    memory_block_t* memory_block = (memory_block_t*) memory_location - 1;

    //Überprüfen ob es wirklich belegter Speicher ist
    if (memory_block->next != MAGIC_NUMBER ||
        memory_block->previous != MAGIC_NUMBER)
    {
        perror("Versuch freien Speicher freizugeben.");
        return;
    }

    //1. Fall: Der gefundene memory block t liegt vor dem aktuell ersten
    //         freien Bereich (gekennzeichnet durch free space list entry point)

    memory_block_t* temp_block = root->free_space_list_entry_point;

    if (memory_block < root->free_space_list_entry_point)
    {
        temp_block->previous = memory_block;
        memory_block->next = temp_block;
        memory_block->previous = NULL;
        root->free_space_list_entry_point = memory_block;
    }

    //2. Fall: Der gefundene memory block t liegt liegt zwischen zwei freien Bereichen.
    temp_block = root->free_space_list_entry_point;
    while (temp_block < memory_block)
    {
        //Wir gehen zur richtigen Stelle
        if (temp_block->next == NULL)
        {
            //3. Fall: Der gefundene memory block t liegt hinter dem letzten freien Speicherbereich.
            temp_block->next = memory_block;
            memory_block->previous = temp_block;
            memory_block->next = NULL;
            return;
        }
        temp_block = temp_block->next;
    }

    if (temp_block->previous != NULL)
    {
        temp_block->previous->next = memory_block;
        memory_block->previous = temp_block->previous;
        temp_block->previous = memory_block;
        memory_block->next = temp_block;
    }


    //5.3
    /*
    temp_block = root->free_space_list_entry_point;
    long puffer = 0;
    long puffer2 = 0;
    while(temp_block->next != NULL)
    {
        printf("%p    ", (void*) temp_block);
        printf("%x\n", (int) temp_block->size);
        temp_block = temp_block->next;
    }
    temp_block = root->free_space_list_entry_point;
    while(temp_block->next != NULL)
    {

        puffer = (temp_block->size / sizeof(memory_block_t)) + 1;
        //printf("Puffer: %ld\n", puffer);
        if (temp_block + puffer == temp_block->next)
        {
            printf("\njaa\n" );



            //2. Fall
            puffer2 = temp_block->next->size + sizeof(memory_block_t);
            temp_block->size += puffer2;

            if (temp_block->next->next != NULL)
            {
                printf("jaa2\n" );
                temp_block->next->next->previous = temp_block;
            }
            temp_block->next = temp_block->next->next;
        }
        //1. Fall
        //nichts tun
        printf("\nPuffer    : %ld\n", puffer);
        printf("Puffer2   : %ld\n", puffer2);
        printf("Puffer2/24: %ld\n", puffer2/24);
        puffer = 0;
        puffer2 = 0;
        temp_block = temp_block->next;
    }


    */
}
/*
puffer += (temp_block->next->size / sizeof(memory_block_t)) + 1;
if (temp_block->next + temp_block->next->size == temp_block->next->next)
{
    //3. Fall
    puffer = temp_block->next->size + temp_block->next->next->size + 2*sizeof(memory_block_t);
    temp_block->size += puffer;

    if (temp_block->next->next != NULL){
        if (temp_block->next->next->next != NULL)
        {
            temp_block->next->next->next->previous = temp_block;
            temp_block->next = temp_block->next->next->next;
        }
        else
        {
            temp_block->next = NULL;
        }
    }

}
*/
