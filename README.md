# Operation Systems

This project was part of the lecture **Operation Systems** at TU Braunschweig

Processing period: Oct. 2018 - Feb. 2019


## Tasks

- Make files
- Linked lists
- Shell functions
- Storage allocation
- Signals
- Multi threading chat server and client
