#include "client.h"

#include <arpa/inet.h>

int client_socket;
int sockfd, newsockfd;
struct sockaddr_in serv_addr, client_addr;


void connect_to_server()
{
    //Initializing of socket file descriptor
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
      perror("ERROR opening socket");
    }
    //sets serv_addr membervariables to 0
    bzero((char *) &serv_addr, sizeof(serv_addr));

    //fill out membervariables of serv_addr struct
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(SERVER_PORT);

    if (connect(sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("ERROR on connection");
    }
}

void handshake()
{
    //sending testing message
    char buf[MAX_MESSAGE_LENGTH] = "Hello, I'm the client";
    if ((int) send(sockfd, buf, strlen(buf), 0) < 0)
    {
        perror("Fehler beim Senden der Nachricht");
    }

    //receiving testing message
    char buf2[MAX_MESSAGE_LENGTH];
    if ((int) recv(sockfd, buf2, MAX_MESSAGE_LENGTH, 0) < 0)
    {
        perror("Fehler beim Empfangen der Nachricht");
    }
    printf("Nachricht erhalten:");
    printf("%s\n", buf2);

}

//-------------------------------------------------------------

void send_message()
{
    //main while loop for never ending prompt
    while(1)
    {
        //user input for making server requests
        char buffer[MAX_MESSAGE_LENGTH] = "";
        prompt_user_input(buffer, MAX_MESSAGE_LENGTH);

        //if user input is empty than run get_request
        if (strcmp(buffer, "\n") == 0)
        {
            get_request();
        } else
        {
            set_request(buffer);
        }
    }

}

//-------------------------------------------------------------

void set_request(char* message)
{
    //Message to Server:
    char message_to_server[MAX_MESSAGE_LENGTH] = "s:";
    strcat(message_to_server, message);
    if ((int) send(sockfd, message_to_server, MAX_MESSAGE_LENGTH, 0) < 0)
    {
        perror("Fehler beim Senden der Nachricht");
    }

    //Answer from Server:
    char buf[MAX_MESSAGE_LENGTH];
    if ((int) recv(sockfd, buf, MAX_MESSAGE_LENGTH, 0) < 0)
    {
        perror("Fehler beim Empfangen der Nachricht");
        exit_client(1);
    }
    if (strcmp(buf, "r:nack") == 0)
    {
        prompt_error();
    }
}

//-------------------------------------------------------------

void get_request()
{
    //request to server:
    char message_to_server[2] = "g:";
    if ((int) send(sockfd, message_to_server, 2, 0) < 0)
    {
        perror("Fehler beim Senden der Nachricht");
    }

    //Answer from Server with actual Message
    char buf[MAX_MESSAGE_LENGTH] = "";
    if ((int) recv(sockfd, buf, MAX_MESSAGE_LENGTH, 0) < 0)
    {
        perror("Fehler beim Empfangen der Nachricht");
    }

    //output to stdout
    print_reply(buf);

    /*
    if (strcmp(buf, "r:nack") == 0)
    {
        perror("R_NACK");
    }
    */
}

//-------------------------------------------------------------

void* read_continously(void* unused)
{
    (void) unused; //Mark variable as used for the compiler :-)

    //write your code here

    //this method should not return so dont care about return value
    return NULL;
}

//-------------------------------------------------------------

void start_reader_thread()
{
    /*
    while (1) {
 clilen = sizeof(cli_addr);
 newsockfd = accept (sockfd, (struct sockaddr *) &cli_addr, &clilen);
 if (newsockfd < 0) error("ERROR on accept");
 bzero(buffer,sizeof(buffer));
 n = read (newsockfd,buffer,sizeof(buffer)-1);
 if (n < 0) error("ERROR reading from socket");
 snprintf (body, sizeof (body),
 "<html>\n<body>\n"
 "<h1>Hallo Webbrowser</h1>\nDeine Anfrage war ...\n"
 "<pre>%s</pre>\n"
 "</body>\n</html>\n", buffer);
 snprintf (msg, sizeof (msg),
 "HTTP/1.0 200 OK\n"
 "Content-Type: text/html\n"
 "Content-Length: %d\n\n%s", strlen (body), body);
 n = write (newsockfd,msg,strlen(msg));
 if (n < 0) error("ERROR writing to socket");
 close (newsockfd);
 }
}
 ...
 w
 */
}
