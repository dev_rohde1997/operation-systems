#include <stdlib.h>
#include <stdio.h>
#include "lili.h"

element_t *head = NULL;


//implement necessary functions here

//TODO: perror(3)
/**
 * @brief insert_element This funtction insertes an element at the end of an list
 * @param value This value will stored as data in the new node
 * @return In Success: Returns the value. Failture: Returns 0.
 */
unsigned int* insert_element(unsigned int value) {
    if (value < 0) {
        printf("Negative values are not allowed!");
        return NULL;
    }
    //Define new node
    //Reserve Storage
    element_t* new_node_malloc = (element_t*) malloc(sizeof (element_t));
    element_t* new_node = new_node_malloc;
    new_node->data = value;
    //OPTIONAL
    new_node->next = NULL;

    //Adding to lsit

    //Empty list
    if (head == NULL) {
        head = new_node;
    } else {
        //Go to the last node
        element_t *temp_node = head;

        while (temp_node->next != NULL) {
            temp_node = temp_node->next;
        }
        temp_node->next = new_node;
    }
    unsigned int* new_node_value_pointer = &new_node->data;
    return new_node_value_pointer;
}

/**
 * @brief print_lili This function prints the full list separated with commas.
 */
void print_lili() {
    printf("print_lili: ");
    if (head != NULL) {
        element_t *temp = head;
        //Iterate though the list and print values
        while (temp->next != NULL) {
            printf("%d, ", temp->data);
            temp = temp->next;
        }
        //Print value of last node
        printf("%d", temp->data);
    }
    printf("\n");

}


/** Removes the last element of the list
 * @brief remove_element
 * @return the value of the deleted node or 0 if the list is empty
 */
unsigned int remove_element() {

    if(head == NULL) {
        printf("WARNING: nothing zo remove, lili is empty\n");
    } else {
        element_t *temp = head;
        //Iterate to the second last element
        while (temp->next != NULL) {
            if(temp->next->next == NULL) {
                break;
            }
            temp = temp->next;
        }
        //Remove last node and free storage
        element_t *remove_obj = temp->next->next;
        unsigned int value = temp->next->data;

        temp->next = NULL;
        free(remove_obj);
        return value;
    }
    return 0;
}
