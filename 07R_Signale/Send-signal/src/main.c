#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <signal.h>

static void print_usage(const char* executable)
{
    fprintf(stderr, "USAGE: %s -s SIGNAL -p PID\n", executable);
    exit(EXIT_FAILURE);
}


int main (int argc, char* argv[])
{

    int arg_count = 0;

    char opt;
    int optarg_s;
    int optarg_p;

    //geht die Option Flags die von getopt zurückgegeben werden durch
    while ((opt = getopt(argc, argv, "sp")) != -1)
    {
        arg_count++;
        switch (opt)
        {
            case 's':
                //to int
                optarg_s = atoi(&opt);
                break;
            case 'p':
                //to int
                optarg_p = atoi(&opt);
                break;
            default:
                print_usage(argv[0]);
        }
    }

    //Abbruchbedingung, falls die Anzhal der Argumente nicht stimmt
    if (arg_count != 2 || argc != 5)
    {
        print_usage(argv[0]);
    }

    //Kill Signal senden
    kill(optarg_p, optarg_s);
  	return EXIT_SUCCESS;
}
