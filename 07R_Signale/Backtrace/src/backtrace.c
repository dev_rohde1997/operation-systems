#include "backtrace.h"
#include <stdio.h>
#include "stdlib.h"
#include <signal.h>
#include <execinfo.h>
#include <errno.h>


//-----------------------------------------------------------------------------
//Gibt eine Liste der aktiven Funktionen mithilfe von Backtrace aus
void print_backtrace(void)
{
    void *buffer[MAXIMAL_BACKTRACE_DEPTH];
    char **strings;

    printf("-----------------------------------------------------\n");
    printf("[DEPTH] FILE(SYMBOL+OFFSET) [ADDRESS]\n");
    printf("-----------------------------------------------------\n");

    int number_of_active_functions = backtrace(buffer, MAXIMAL_BACKTRACE_DEPTH);

    strings = backtrace_symbols(buffer, number_of_active_functions);
    if (strings == NULL)
    {
        perror("backtrace_symbols");
        exit(EXIT_FAILURE);
    }

    //Ausgabe des Stacks
    for (int i = 0; i < number_of_active_functions; i++)
    {
        printf("[%d] %s\n", i, strings[i]);
    }

    free(strings);

}

//-----------------------------------------------------------------------------
//ruft print_backtrace() auf und terminiert den Prozess mit der entsprechenden signal_number
void signal_handler(int signal_number)
{
    if (errno != EINTR)
    {
      printf("Received Signal: %d\n", signal_number);
      print_backtrace();
      exit(signal_number);
    } else
    {
      perror("Interrupted System Call: ");
    }

}

//-----------------------------------------------------------------------------
//fängt die Signale ab und ruft signal_number mit der entsprechenden Signal Nummer auf
void initialize_signal_handler(void)
{
    //Interrupt (durch Tastatur
    signal(SIGINT, signal_handler);
    //Ungültige Maschinenanweisung
    signal(SIGQUIT, signal_handler);
    //Abnormale Prozessterminierung
    signal(SIGABRT, signal_handler);
    //Floating Point Exception
    signal(SIGFPE, signal_handler);
    //Segmentation Fault
    signal(SIGSEGV, signal_handler);
    //Terminierung
    signal(SIGTERM, signal_handler);
    //Stop durch Tastatur
    signal(SIGCONT, signal_handler);
}
